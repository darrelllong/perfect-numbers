CFLAGS=-Wall -Wextra -pedantic -O3
CC=clang

parfait	:	parfait.o sieve.o bv.o
	$(CC) -o parfait parfait.o sieve.o bv.o -lm

bv.o	:	bv.c bv.h
	$(CC) $(CFLAGS) -c bv.c

sieve.o	:	sieve.c sieve.h
	$(CC) $(CFLAGS) -c sieve.c

parfait.o	:	parfait.c bv.h sieve.h
	$(CC) $(CFLAGS) -c parfait.c

clean	:
	rm -f parfait *.o
