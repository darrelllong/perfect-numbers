# ifndef _DEBUG_H
# define _DEBUG_H
# define DEBUG(x) \
{ if (DO_DEBUG) printf("DEBUG: %s, %s(%d)\n", x, __func__, __LINE__); } 
# endif
