# include <stdio.h>
# include <stdlib.h>
# include "bv.h"
# include "sieve.h"

# define odd(b)	(b & 0x1)

// Compute b^k using the method of repeated squaring.

uint64_t power(uint64_t b, uint64_t k)
{
	if (k == 0)      { return 1; }
	else if (k == 1) { return b; }
	else if (odd(k))
	{
		return b * power(b * b, k >> 1);
	}
	else
	{
		return power(b * b, k >> 1);
	}
}

// Compute all possible products using valid exponents resulting from a prime
// factorization.

void product(uint64_t s, uint64_t b[], uint64_t x[], uint32_t  p, uint32_t l, uint64_t v[], uint32_t *m)
{
	if (p < l)
	{
		for (uint32_t i = 0; i <= x[p]; i += 1)
		{
			product(s * power(b[p], i), b, x, p + 1, l, v, m);
		}
	}
	else
	{
		 v[*m] = s; *m += 1; // At the end we add one more.
	}
	return;

}

// Reduce a sorted sequence of factors to a list of base and exponent pairs.

uint32_t reduce(uint64_t a[], uint32_t l, uint64_t b[], uint64_t x[])
{
	uint32_t	m = 1;

	b[0] = a[0]; // First base
	x[0] = 1;

	for (uint32_t i = 1; i < l; i += 1)
	{
		if (a[i] != b[m - 1])
		{
			b[m] = a[i]; // Next base found
			x[m] = 1;
			m += 1;
		}
		else
		{
			x[m - 1] += 1;
		}
	}
	return m;
}

// Factor an integer into its constituent primes.

uint32_t factors(bitV *primes, uint64_t c, uint64_t v[])
{
	uint32_t count = 0;

	while (c % 2 == 0) { v[count] = 2; count += 1; c /= 2; }

	for (uint32_t i = 3; i <= c; i += 2)
	{
		while (valBit(primes, i) && c % i == 0)
		{
			c /= i;
			v[count] = i;
			count += 1;
		}
	}
	return count;
}

# define SWAP(x, y)	{ uint64_t t = x; x = y; y = t; }

uint32_t partition(uint64_t a[], int32_t low, int32_t high)
{
	uint32_t	storeIndex;
	uint32_t	pivotIndex = (low + high) / 2, 
			pivotValue = a[pivotIndex];

	SWAP(a[pivotIndex], a[high]);
	storeIndex = low;
	for (int i = low; i < high; i += 1) {
		if (a[i] < pivotValue) {
			SWAP(a[i], a[storeIndex]);
			storeIndex += 1;
		}
	}
	SWAP(a[storeIndex], a[high]);
	return storeIndex;
}

void quickSort(uint64_t a[], int32_t low, int32_t high)
{
	if (low < high) {
		int	p = partition(a, low, high);

		quickSort(a, low,   p - 1);
		quickSort(a, p + 1, high);
	}
	return;
}

// Sum a vector.

uint64_t sum(uint64_t a[], uint32_t l)
{
        uint64_t	s = 0;

        for (uint32_t i = 0; i < l; i += 1)
        {
                s += a[i];
        }
        return s;
}

// Casting out nines.

uint64_t digitalRoot(uint64_t k) { return 1 + (k - 1) % 9; }

# define MAX 	1000	// BFN!
# define PRIMES	50000000
# define EE	5	// We only need this many perfect numbers.

# define MASK(n) (((uint64_t) ~0x0)>>(8*sizeof(uint64_t)-n) & ((uint64_t) ~0x0))

// Things we know about perfect numbers:
// (a) 6 is an exception;
// (b) Digital root (casting out nines) is 1;
// (c) They are even, and each subsequent one has twice as many zeros in its
//     binary representation than the one before.

int candidate(uint64_t n, uint32_t e)
{
	return (n < 7 || ((digitalRoot(n) == 1) && (MASK(2 * e) & n) == 0));
}

int main(void)
{
	uint64_t	facts[MAX], base[MAX], exp[MAX], proper[MAX];
	bitV		*primes = newVec(PRIMES);

	uint32_t	e = 0;
	
	sieve(primes);

	for (uint64_t i = 2; e < EE && i < PRIMES; i+= 1)
	{
		uint32_t	f = factors(primes, i, facts);

		if (valBit(primes, i))
		{
			printf("%ld P\n", i);
		}
		else
		{
			printf("%ld C:", i);
			for (uint32_t j = 0; j < f; j += 1)
			{
				printf(" %ld", facts[j]);
			}
			printf("\n");
			if (candidate(i, e))
			{
				uint32_t	m = reduce (facts, f, base, exp);
				uint32_t	n = 0;

				product  (1, base, exp, 0, m, proper, &n);
				quickSort(proper, 0, n - 1);
			if (sum(proper, n - 1) == i)
			{
				e += 1;
				printf("%ld E:", i);
				for (uint32_t j = 0; j < n - 1; j += 1)
				{
					printf(" %ld", proper[j]);
				}
			printf("\n");
			}
		}
		}
		fflush(stdout);
	}
	delVec(primes);
	return 0;
}
