# include <stdint.h>
# include <stdio.h>

# include "bv.h"
# include "sieve.h"
# include "debug.h"

uint32_t bitCount(bitV *b)
{
	uint32_t c = 0;

DEBUG("Count bits");
	for (uint32_t i = 0; i < lenVec(b); i += 1)
	{
		c += valBit(b, i);
	}

	return c;
}

uint32_t nextPrime(bitV *v, uint32_t p)
{
	for (uint32_t i = p + 1; i < v->l; i += 1)
	{
		if (valBit(v, i))
		{
			return i;
		}
	}
	return 0;
}

void printVec(bitV *v)
{
	for (uint32_t i = 0; i < v->l; i += 1)
	{
		if (valBit(v, i)) { printf("1"); }
		else              { printf("0"); }
	}
	printf("\n");
}

int main(void)
{
	uint32_t p = 1;
	uint32_t n;

	printf("n = "); fflush(stdout);
	scanf ("%u", &n);

	bitV *v = newVec(n); sieve(v);

	while ((p = nextPrime(v, p)) > 0) { printf("%u\n", p); }

	if (n < 100) { printVec(v); }
	printf("Number of primes %u\n", bitCount(v));
	printf("Prime density %lf\n", (double) bitCount(v) / (double) v->l);

	delVec(v);
	return 0;
}
