# include <stdlib.h>
# include <stdint.h>
# include "bv.h"

bitV *newVec(uint32_t l)
{
	bitV *v = (bitV *) malloc(sizeof(bitV));
	v->v = (uint8_t *) malloc(l * sizeof(uint8_t) / 8 + 1);
	v->l = l;
	return v;
}

void delVec(bitV *v)
{
	free(v->v); free(v);
	return;
}

void oddVec(bitV *v)
{
	for (uint32_t i = 0; i < v->l / 8 + 1; i += 1)
	{
		v->v[i] = 0xaa;
	}
	return;
}

void setBit(bitV *v, uint32_t k)
{
	v->v[k / 8] |= (0x1 << k % 8);
	return;
}

void clrBit(bitV *v, uint32_t k)
{
	v->v[k / 8] &= ~(0x1 << k % 8);
	return;
}

uint8_t valBit(bitV *v, uint32_t k)
{
	return (v->v[k / 8] & (0x1 << k % 8)) != 0;
}

uint32_t lenVec(bitV *v)
{
	return v->l;
}
